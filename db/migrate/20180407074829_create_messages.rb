class CreateMessages < ActiveRecord::Migration[5.1]
  def change
    create_table :messages do |t|
      t.string :file
      t.string :text
      t.references :car, foreign_key: true
      t.integer "length", :null => false, :default => 0

      t.timestamps
    end
  end
end
