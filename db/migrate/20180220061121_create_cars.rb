class CreateCars < ActiveRecord::Migration[5.1]
  def change
    create_table :cars do |t|
      t.string :number
      t.string :username
      t.decimal "latitude", :precision => 19, :scale => 17, :default => 0.0
      t.decimal "longitude", :precision => 19, :scale => 17, :default => 0.0
      t.datetime :last_message_date, :null => false, default: -> {'CURRENT_TIMESTAMP'}

      t.timestamps
    end
  end
end
