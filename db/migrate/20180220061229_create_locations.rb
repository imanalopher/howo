class CreateLocations < ActiveRecord::Migration[5.1]
  def change
    create_table :locations do |t|

      t.string "username", :username
      t.decimal "latitude", :precision => 19, :scale => 17, :default => 0.0
      t.decimal "longitude", :precision => 19, :scale => 17, :default => 0.0
      t.references :car, foreign_key: true

      t.timestamps
    end
  end
end
