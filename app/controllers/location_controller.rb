class LocationController < ApplicationController
  protect_from_forgery with: :null_session
  skip_before_action :verify_authenticity_token

  def index
    @cars = Car.all
    render :json => @cars
  end

  def record
    Car.update(params[:location][:id], :longitude => params[:location][:longitude], :latitude => params[:location][:latitude])
    @cars = Car.all
    @location = Location.new(location_params)
    if @location.save
      render :json => { :cars => @cars }
    else
      render :json =>  { error: "Couldn't create locations", status: 500 }
    end
  end

  def report
    @cars = Car.all
  end

  def report_post
    @cars = Car.all
    @driver = Car.find(report_params[:car_id])
    @start = report_params[:picker_start]
    @finish = report_params[:picker_finish]
    @locations = Location.select("latitude as lat, longitude as lng").where("car_id = ? AND created_at >= ? AND created_at <= ?", report_params[:car_id], report_params[:picker_start], report_params[:picker_finish])
  end

  private
    def location_params
      params.require(:location).permit(:username, :longitude, :latitude, :car_id)
    end

    def report_params
      params.permit(:car_id, :picker_start, :picker_finish)
    end

    def car_params
      params.require(:location).permit(:id, :longitude, :latitude)
    end
end
