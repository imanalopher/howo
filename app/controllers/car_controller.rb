class CarController < ApplicationController
  protect_from_forgery with: :null_session

  def register
    @car = Car.new(car_params)
    if @car.save
      render :json => Car.all
    else
      render :json =>  { error: "Couldn't create car", status: 500 }
    end
  end

  def login
    @car = Car.select(:id, :number, :username, :latitude, :longitude).find_by(car_params)
    if @car.present?
      render :json => @car, status: 200
    else
      render :json =>  { error: "Couldn't find any car", status: 500 }, status: 400
    end
  end

  def car
    @cars = Car.all
  end

  def new
    @car = Car.new
  end

  def create
    @car = Car.new(car_params)
    if @car.save
      redirect_to admin_user_path
    else
      render admin_car_new_path
    end
  end

  def delete
    @car = Car.find(params[:id])
    if @car.destroy
      redirect_to admin_user_path
    else
      render admin_user_path
    end
  end

  def edit
    @car = Car.find(params[:id])
  end

  def update
    @car = Car.find(params[:id])
    @car.update(car_params)
    redirect_to admin_user_path
  end

  private
    def car_params
      params.require(:car).permit(:number, :username)
    end
end
