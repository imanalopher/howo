class AdminController < ApplicationController
  protect_from_forgery with: :null_session

  def index
    @admins = Admin::all
    render :json => @admins
  end
  def create
    username = (0...8).map { (65 + rand(16)).chr }.join
    @admins = Admin.create(:username => username, :password => username, :lat => 0.0, :lng => 0.0)
    if @admins.save
      render :json => @admins
    else
      render :json => { error: "Couldn't create car", status: 500 }
    end
  end
  def login
    @admin = Admin.select(:id, :username, :password, :lat, :lng).find_by(admin_login_params)
    if @admin.present?
      render :json => @admin
    else
      render :json => { error: "Couldn't find any admin", status: 500 }, status: 400
    end
  end

  private
    def admin_login_params
      params.require(:admin).permit(:username, :password)
    end
end
