class MessageController < ApplicationController
  protect_from_forgery with: :null_session
  skip_before_action :verify_authenticity_token

  def index
    @messages = Message.all
  end

  def last_message
    @user = Car.find(last_message_params[:car_id])
    @messages = Message.select(:id, :car_id, :file, :created_at, :length).where('messages.created_at > :last_user_message and messages.car_id != :car_id', :last_user_message => @user.last_message_date, :car_id => @user.id)
    if @messages.length > 0
      @user.last_message_date = @messages.last.created_at
      if @user.save
        render :json => @messages
      else
        render :json => {:status => 500, :message => 'Can\'t update'}
      end
    else
      render :json => {}
    end
  end

  def create_file_message

  end

  def save_file
    @driver = Car.select(:id, :username).where(id: message_params[:car_id]).first
    time_now = Time.now.strftime("%d_%m_%Y_%H_%M_%S")
    file_name = @driver.username + "_#{time_now}"
    file = message_params[:file]
    original_file_name = file.original_filename  if  (file !='')
    file_type = original_file_name.split('.').last
    file_name = "#{file_name}." + file_type

    if ['mp3', 'mp4'].include?(file_type)
      Message.save_file(message_params[:file], file_name)
      @message = Message.new(:file => file_name, :car_id => message_params[:car_id], :length => message_params[:length])
      if @message.save
        render :json => @message
      end
    else
      render :json => {:status => 'Error', :code => 500 }
    end
  end

  private

  def message_params
    params.permit(:file, :car_id, :length)
  end

  def last_message_params
    params.permit(:car_id, :datetime)
  end

end
