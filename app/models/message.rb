class Message < ApplicationRecord
  belongs_to :car

  attr_accessor :upload

  def self.save_file(upload, file_name)

    file = upload.read
    image_root = "public/messages_images"

    unless File.directory?(image_root)
      Dir.mkdir(image_root)
    end

    File.open(image_root + "/" + file_name, "wb")  do |f|
      f.write(file)
    end

  end
end
