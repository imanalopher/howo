class Car < ApplicationRecord
  has_many :locations, dependent: :destroy
  has_many :messages, dependent: :destroy
end
