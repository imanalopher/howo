Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'location#index'

  get 'admin/report' => 'location#report', as: :user_location_report
  post 'admin/report' => 'location#report_post', as: :user_location_report_post
  post '' => 'location#record'
  post 'admin/message/last_message' => 'message#last_message'
  post 'admin/message/file' => 'message#save_file'
  post 'register' => 'car#register'
  post 'login' => 'car#login'
  get 'admin' => 'admin#index'
  get 'admin/create' => 'admin#create'
  post 'admin/login' => 'admin#login'
  get 'admin/user' => 'car#car'
  get 'admin/user/:id/edit' => 'car#edit', as: :admin_car_edit
  post 'admin/user/:id/update' => 'car#update', as: :admin_car_update
  get 'admin/user/new' => 'car#new', as: :admin_car_new
  post 'admin/user/create' => 'car#create', as: :admin_car_create
  delete 'admin/user/:id/delete' => 'car#delete', as: :admin_car_delete
  get 'admin/map' => 'map#index', as: :admin_google_map
end
